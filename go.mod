module bettor

go 1.19

require (
	fyne.io/fyne/v2 v2.2.4
	github.com/arewabolu/csvmanager v1.4.6
	github.com/arewabolu/trademath v0.0.4
	golang.org/x/exp v0.0.0-20230118134722-a68e582fa157
	gonum.org/v1/gonum v0.12.0

)

require (
	fyne.io/systray v1.10.1-0.20220621085403-9a2652634e93 // indirect
	github.com/arewabolu/GoHaskell v0.0.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fredbi/uri v0.0.0-20221012073901-fb871453c6d3 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/fyne-io/gl-js v0.0.0-20220802150000-8e339395f381 // indirect
	github.com/fyne-io/glfw-js v0.0.0-20220517201726-bebc2019cd33 // indirect
	github.com/fyne-io/image v0.0.0-20221020213044-f609c6a24345 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/goki/freetype v0.0.0-20220119013949-7a161fd3728c // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/jsummers/gobmp v0.0.0-20151104160322-e2ba15ffa76e // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sajari/regression v1.0.1
	github.com/srwiley/oksvg v0.0.0-20221011165216-be6e8873101c // indirect
	github.com/srwiley/rasterx v0.0.0-20220730225603-2ab79fcdd4ef // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/tevino/abool v1.2.0 // indirect
	github.com/yuin/goldmark v1.5.2 // indirect
	golang.org/x/image v0.1.0 // indirect
	golang.org/x/mobile v0.0.0-20221110043201-43a038452099 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	honnef.co/go/js/dom v0.0.0-20221001195520-26252dedbe70 // indirect
)

replace golang.org/x/mobile => github.com/fyne-io/mobile v0.0.0-20191204153723-f14fb9562406

//replace github.com/arewabolu/csvmanager v1.4.2 => github.com/arewabolu/csvmanager v1.4.3
